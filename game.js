var editorPalatte = new Palatte(
  new Color(0, 0, 0, 255),

  new Color(255, 0, 0, 255),
  new Color(0, 255, 0, 255),
  new Color(0, 0, 255, 255),

  new Color(255, 255, 0, 255),
  new Color(0, 255, 255, 255),
  new Color(255, 0, 255, 255),

  new Color(255, 127, 0, 255),
  new Color(0, 255, 127, 255),
  new Color(127, 0, 255, 255),

  new Color(127, 255, 0, 255),
  new Color(0, 127, 255, 255),
  new Color(255, 0, 127, 255),

  new Color(255, 255, 255, 255)
);

function init() {
  document.title = "Electron Game Framework Demo";

  gameArea.components.push(
    new Component("delta", { }, null,
      function(delta, tickDelta, interpolationFactor) {
        ctx = gameArea.context;
        ctx.fillStyle = "black";
        ctx.font = "20px sans-serif";
        ctx.fillText(delta, 10, 20);
        ctx.fillText(tickDelta, 10, 40);
        ctx.fillText(interpolationFactor, 10, 60);
				//console.log(tickDelta, interpolationFactor);
      }
    )
  );

  gameArea.components.push(
    new Component("move",
      { width: 30, height: 30, color: "cyan", x: 10, y: 120, speed: 5 },
        function() {
        if (gameArea.key(37) || gameArea.key(charCode("A")))
          {this.params.x += 0 - this.params.speed; }
        if (gameArea.key(39) || gameArea.key(charCode("D")))
          {this.params.x += this.params.speed; }
        if (gameArea.key(38) || gameArea.key(charCode("W")))
          {this.params.y += 0 - this.params.speed; }
        if (gameArea.key(40) || gameArea.key(charCode("S")))
          {this.params.y += this.params.speed; }
      },
      function() {
        ctx = gameArea.context;
        ctx.fillStyle = this.params.color;
        ctx.fillRect(this.params.x, this.params.y, this.params.width, this.params.height);
      }
    )
  );

  gameArea.components.push(
    new Component("clickTracker", {}, null,
      function() {
        gameArea.cursor.clickStarts.forEachSet(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "yellow";
          ctx.beginPath();
          ctx.moveTo(value.x, value.y);
          ctx.lineTo(gameArea.cursor.x, gameArea.cursor.y);
          ctx.stroke();

          ctx.fillStyle = "blue";
          ctx.fillRect(value.x - 3, value.y - 3, 5, 5);
        }, 5);

        gameArea.cursor.clicks.forEach(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "blue";
          ctx.beginPath();

          ctx.moveTo(value.start.x, value.start.y);
          if (value.drag) {
            value.drag.forEach(function(item) {
              gameArea.context.lineTo(item.x, item.y);
            });
          }
          ctx.lineTo(value.end.x, value.end.y);

          ctx.stroke();

          ctx.fillStyle = "green";
          ctx.fillRect(value.start.x - 8, value.start.y - 8, 15, 15);

          ctx.fillStyle = "red";
          ctx.fillRect(value.end.x - 8, value.end.y - 8, 15, 15);
        });

        //gameArea.cursor.clicks = [];
      }
    )
  );

  gameArea.components.push(
    new Component("touchTracker", {}, null,
      function() {
        gameArea.touch.clickStarts.forEachSet(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "yellow";
          ctx.beginPath();
          ctx.moveTo(value.x, value.y);
          ctx.lineTo(gameArea.touch.x, gameArea.touch.y);
          ctx.stroke();

          ctx.fillStyle = "blue";
          ctx.fillRect(value.x - 3, value.y - 3, 5, 5);
        }, 5);

        gameArea.touch.clicks.forEach(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "blue";
          ctx.beginPath();

          ctx.moveTo(value.start.x, value.start.y);
          if (value.drag) {
            value.drag.forEachSet(function(item) {
              gameArea.context.lineTo(item.x, item.y);
            });
          }
          ctx.lineTo(value.end.x, value.end.y);

          ctx.stroke();

          ctx.fillStyle = "green";
          ctx.fillRect(value.start.x - 8, value.start.y - 8, 15, 15);

          ctx.fillStyle = "red";
          ctx.fillRect(value.end.x - 8, value.end.y - 8, 15, 15);
        });

        //gameArea.touch.clicks = [];
      }
    )
  );

  gameArea.components.push(
    new Component("mouse",
      { width: 5, height: 5, x: 10, y: 120 },
      null,
      function() {
        this.params.x = gameArea.cursor.x - 3;
        this.params.y = gameArea.cursor.y - 3;

        ctx = gameArea.context;
        if (gameArea.cursor.click) {
          ctx.fillStyle = "magenta";
        }
        else {
          ctx.fillStyle = "black";
        }
        ctx.fillRect(this.params.x, this.params.y, this.params.width, this.params.height);
      }
    )
  );

  gameArea.components.push(
    new Component("keys",
      { x: 10, y: 700 },
      null,
      function() {
        let keyboardOutput = "";
        gameArea.keyboard.keys.forEach(function(value) {
          keyboardOutput += String.fromCharCode(value);
        });

        if (keyboardOutput != "") {
          ctx = gameArea.context;
          ctx.fillStyle = "black";
          ctx.font = "20px sans-serif";
          ctx.fillText(keyboardOutput + " (" + [...gameArea.keyboard.keys].join(' ') + ")", this.params.x, this.params.y);
        }
      }
    )
  );

  gameArea.start();
}
