const { app, BrowserWindow } = require('electron');

const fs = require('fs');
const path = require('path');

let mainWindow;

app.on('window-all-closed', () => {
  app.quit();
});

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    useContentSize: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    },
    width: 1280,
    height: 720,
    show: false,
    resizable: false,
    maximizable: false,
    fullscreenable: false
  });
  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })
  mainWindow.loadURL(`file://${__dirname}/index.html`);
});
